// C Program to design a shell in Linux
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<fcntl.h>
#include<readline/readline.h>
#include<readline/history.h>
#include<errno.h>

#include "Parsed.h"
#include "Utils.h"
#include "Exec.h"
#include "Builtin.h"

#define MAXCOM 1000 // max number of letters to be supported
#define MAXLIST 100 // max number of commands to be supported

#define clear() printf("\033[H\033[J")

/**
 * Analyze the command to see if it contains a double pipe
 * 
 * @param str            inital command
 * @param strDoublePipe  table with the different parts of the command
 * @return               true(1) if we validate the presence of a double pipe else false(0)
 */
int parseDoublePipe(char* str, char** strdoublein)
{
    int i;
    for (i = 0; i < 2; i++) {
        strdoublein[i] = strtok_r(str, "||", &str);
        if (strdoublein[i] == NULL)
            break;
    }
    
    if (strdoublein[1] == NULL)
        return 0; // returns zero if no || is found.
    else {
        return 1;
    }
}


/**
 * Analyze the command to see if it contains a double and
 * 
 * @param str            inital command
 * @param strdoubleand   table with the different parts of the command
 * @return               true(1) if we validate the presence of a double and else false(0)
 */
int parseDoubleAnd(char* str, char** strdoubleand)
{
    int i;
    for (i = 0; i < 2; i++) {
        strdoubleand[i] = strtok_r(str, "&&", &str);
        if (strdoubleand[i] == NULL)
            break;
    }

    if (strdoubleand[1] == NULL)
        return 0; // returns zero if no && is found.
    else {
        return 1;
    }
}


/**
 * Analyze the command to see if it contains a double redirection in
 * 
 * @param str            inital command
 * @param strdoublein    table with the different parts of the command
 * @return               true(1) if we validate the presence of a double redirection in else false(0)
 */
int parseDoubleRedirectIn(char* str, char** strdoublein)
{
    int i;
    for (i = 0; i < 2; i++) {
        strdoublein[i] = strtok_r(str, "<<", &str);
        if (strdoublein[i] == NULL)
            break;
    }
    if (strdoublein[1] == NULL)
        return 0; // returns zero if no >> is found.
    else {
        return 1;
    }
}

/**
 * Analyze the command to see if it contains a redirection in
 * 
 * @param str            inital command
 * @param strredin       table with the different parts of the command
 * @return               true(1) if we validate the presence of redirection in else false(0)
 */
int parseRedirectIn(char* str, char** strredin)
{
    int i;
    for (i = 0; i < 4; i++) {
        strredin[i] = strtok_r(str, ">>", &str);
        if (strredin[i] == NULL)
            break;
    }
    if (strredin[1] == NULL)
        return 0; // returns zero if no pipe is found.
    else {
        return 1;
    }
}

/**
 * Analyze the command to see if it contains a redirection out
 * 
 * @param str            inital command
 * @param strredout      table with the different parts of the command
 * @return               true(1) if we validate the presence of redirection out else false(0)
 */
int parseRedirectOut(char* str, char** strredout)
{
    int i;
    for (i = 0; i < 2; i++) {
        strredout[i] = strtok_r(str, "<<", &str);
        //strredout[i] = strsep(&str, ">");
        if (strredout[i] == NULL)
            break;
    }
    if (strredout[1] == NULL)
        return 0; // returns zero if no pipe is found.
    else {
        return 1;
    }
}

/**
 * Analyze the command to see if it contains a pipe
 * 
 * @param str            inital command
 * @param strpiped       table with the different parts of the command
 * @return               true(1) if we validate the presence of a pipe else false(0)
 */
int parsePipe(char* str, char** strpiped)
{
    int i;
    for (i = 0; i < 2; i++) {
        strpiped[i] = strsep(&str, "|");
        if (strpiped[i] == NULL)
            break;
    }
    if (strpiped[1] == NULL)
        return 0; // returns zero if no pipe is found.
    else {
        return 1;
    }
}

/**
 * function for parsing command words
 * 
 * @param str            inital command
 * @param parsed         table with the different parts of the command
 */
void parseSpace(char* str, char** parsed)
{
    int i;
    for (i = 0; i < MAXLIST; i++) {
        parsed[i] = strsep(&str, " ");
        if (parsed[i] == NULL)
            break;
        if (strlen(parsed[i]) == 0)
            i--;
    }
}