#ifndef UTILS_H_
#define UTILS_H_

int takeInput(char* str);
void init_shell();
void writeOnHistoryFile(char *command);
void printHistory();
void closeAndDeleteFile();
void printDir();
void openHelp();
//int processString(char* str, char** parsed, char** parsedpipe, char** strredin,char** strredout);

#endif // UTILS_H_