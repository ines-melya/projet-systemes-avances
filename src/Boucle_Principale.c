// C Program to design a shell in Linux
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<readline/readline.h>
#include<readline/history.h>
#include<errno.h>

#include "Utils.h"
#include "Parsed.h"
#include "Exec.h"
#include "Builtin.h"

#define MAXCOM 1000 // max number of letters to be supported
#define MAXLIST 100 // max number of commands to be supported

// Clearing the shell using escape sequences
#define clear() printf("\033[H\033[J")

int main()
{
    char inputString[MAXCOM], * parsedArgs[MAXLIST];

    int i;
    int count = 0;
    int countOut = -1;
    char* parsedIn[MAXLIST];
    char* parsedArgsPiped[MAXLIST];
    char* parsedDoubleAnd1[MAXLIST];
    char* parsedDoubleAnd2[MAXLIST];
    char* parsedDoublePipe1[MAXLIST];
    char* parsedDoublePipe2[MAXLIST];
    char* parsedOut[MAXLIST];
    init_shell();
    int execFlag = 0;
    unsigned char buff[4096];
    while (1) {
        // print shell line
        printDir();
        // take input
        if (takeInput(inputString))
            continue;
        // Search which type (simple or double) is in the input from the user.
        for (i = 0; i < 100; i++) {
            if (inputString[i] == '>') {
                count = count + 1;
            }
            if (inputString[i] == '<') {
                countOut = countOut + 1;
            }
        }

        execFlag = processString(inputString, parsedArgs, parsedArgsPiped, parsedIn, parsedOut,parsedDoubleAnd1, parsedDoubleAnd2, parsedDoublePipe1, parsedDoublePipe2);
        // execflag returns zero if there is no command
        // or it is a builtin command,
        // 1 if it is a simple command
        // 2 if it is including a pipe.
        // 4 if RedirectionIn
        // 5 if redirection Out
        // 14 for a double And
        // 15 for a double Pipe
        char* output = parsedIn[0];
        char* outputf = parsedIn[0];
        // execute
        if (execFlag == 1)
            execArgs(parsedArgs);
        if (execFlag == 2)
            execArgsPiped(parsedArgs, parsedArgsPiped);
        if (execFlag == 4)
            RedirectionIn(inputString, output, count);
        if (execFlag == 5)
            RedirectionOut(inputString, outputf, countOut);
        if(execFlag == 14)
            execDoubleAnd(parsedDoubleAnd1, parsedDoubleAnd2);
        if(execFlag == 13)
            execDoublePipe(parsedDoublePipe1, parsedDoublePipe2);

        count = 0;
        countOut = 0;
    }
    closeAndDeleteFile();

    return 0;
}