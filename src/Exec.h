#ifndef EXEC_H_
#define EXEC_H_

void execArgs(char** parsed);
void execArgsPiped(char** parsed, char** parsedpipe);
void RedirectionIn(char* parsed, char* strredoublein, int f);
void RedirectionOut(char* parsed, char* strredout, int f);
void execDoublePipe(char** strDoublePipe1, char** strDoublePipe2);
void execDoubleAnd(char** strDoubleAnd1,char** strDoubleAnd2);
int processString(char* str, char** parsed, char** parsedpipe, char** strredin, char** strredout, char** stringDoubleAnd1, char** stringDoubleAnd2, char** stringDoublePipe1, char** stringDoublePipe2);

#endif // EXEC_H_
