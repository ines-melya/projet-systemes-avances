// C Program to design a shell in Linux
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<fcntl.h>
#include<readline/readline.h>
#include<readline/history.h>
#include<errno.h>

#include "Exec.h"
#include "Utils.h"
#include "Parsed.h"
#include "Builtin.h"

#define MAXCOM 1000 // max number of letters to be supported
#define MAXLIST 100 // max number of commands to be supported

#define clear() printf("\033[H\033[J")

/**
 * Function where the system command is executed
 * 
 * @param parsed         parsed command.
 */
void execArgs(char** parsed)
{
    // Forking a child
    pid_t pid = fork();

    if (pid == -1) {
        printf("\nFailed forking child..");
        return;
    }
    else if (pid == 0) {
        if (execvp(parsed[0], parsed) < 0) {
            printf("\nCould not execute command..");
        }
        exit(0);
    }
    else {
        // waiting for child to terminate
        wait(NULL);
        return;
    }
}

/**
 * Function where the piped system commands is executed
 * 
 * @param parsed         command
 * @param parsedpipe     pipe
 */
void execArgsPiped(char** parsed, char** parsedpipe)
{
    // 0 is read end, 1 is write end
    int pipefd[2];
    pid_t p1, p2;

    if (pipe(pipefd) < 0) {
        printf("\nPipe could not be initialized");
        return;
    }
    p1 = fork();
    if (p1 < 0) {
        printf("\nCould not fork");
        return;
    }

    if (p1 == 0) {
        // Child 1 executing..
        // It only needs to write at the write end
        close(pipefd[0]);
        dup2(pipefd[1], STDOUT_FILENO);
        close(pipefd[1]);

        if (execvp(parsed[0], parsed) < 0) {
            printf("\nCould not execute command 1..");
            exit(0);
        }
    }
    else {
        // Parent executing
        p2 = fork();

        if (p2 < 0) {
            printf("\nCould not fork");
            return;
        }

        // Child 2 executing..
        // It only needs to read at the read end
        if (p2 == 0) {
            close(pipefd[1]);
            dup2(pipefd[0], STDIN_FILENO);
            close(pipefd[0]);
            if (execvp(parsedpipe[0], parsedpipe) < 0) {
                printf("\nCould not execute command 2..");
                exit(0);
            }
        }
        else {
            // parent executing, waiting for two children
            close(pipefd[1]);
            wait(NULL);
            wait(NULL);
        }
    }
}

/**
 * Execution of a command with redirection In
 * 
 * @param parsed         parsed command
 * @param strredoublein  output
 * @param f              count value
 */
void RedirectionIn(char* parsed, char* strredoublein, int f)
{
    int input = open(parsed, O_RDONLY);
    int nbread = 0;
    int output;
    unsigned char buff[4096];
    if (f != 2) {
        output = open(strredoublein, O_RDWR | O_CREAT, 00777);
        while ((nbread = read(input, buff, 4095)) > 0) {
            write(output, buff, nbread);
        }
        close(output);
    }
    else {
        output = open(strredoublein, O_RDWR | O_APPEND, 00777);
        while ((nbread = read(input, buff, 4095)) > 0) {
            write(output, buff, nbread);
        }close(output);   
    }
    f = 0;
    close(input);
}

/**
 * execution of a command with redirection out
 * 
 * @param parsed         command
 * @param strredout      input
 * @param f              count value
 */
void RedirectionOut(char* parsed, char* strredout, int f)
{
    int output;
    int nbread = 0;
    char* file = strredout;
    int input = open(file, O_RDONLY);
    unsigned char buff[4096];
    if (f != 2) {
        output = open(parsed, O_RDWR | O_CREAT, 00777);
        while ((nbread = read(input, buff, 4095)) > 0) {
            write(output, buff, nbread);
        }
        close(output);
    }
    else {
        output = open(parsed, O_RDWR | O_APPEND, 00777);
        while ((nbread = read(input, buff, 4095)) > 0) {
            write(output, buff, nbread);
        }close(output);
    }
    f = 0;
    close(input);
}


/**
 * Execution of a command with ||
 * 
 * @param strDoublePipe1 first command
 * @param strDoublePipe2 second command
 */
void execDoublePipe(char** strDoublePipe1, char** strDoublePipe2) {
  
    int f = fork();
    int ret = -1;

    if (f == -1) {
        printf("\nFailed forking child..");
        return;
    }
    
    else if (f == 0) {
        if (ret = execvp(strDoublePipe1[0], strDoublePipe1) < 0) {
            ret = execvp(strDoublePipe2[0], strDoublePipe2);
        }   
    }  
    wait(NULL);
    return;
}


/**
 * Execution of a command with ||
 * 
 * @param strDoubleAnd1  first command
 * @param strDoubleAnd2  second command
 */
void execDoubleAnd(char** strDoubleAnd1, char** strDoubleAnd2) {

    int f = fork();
    int ret =-1 ;
   if (f == -1) {
        printf("\nFailed forking child..");
        return;
    }

     if (f == 0) {
         printf("\n %d ret", ret);
        if ((ret = execvp(strDoubleAnd1[0], strDoubleAnd1)) == 0) {
            ret = execvp(strDoubleAnd2[0], strDoubleAnd2);
        }
    }
    wait(NULL);
    return;
}

int processString(char* str, char** parsed, char** parsedpipe, char** strredin, char** strredout, char** stringDoubleAnd1, char** stringDoubleAnd2, char** stringDoublePipe1, char** stringDoublePipe2)
{
    writeOnHistoryFile(str);
    int piped = 0;
    int in = 1;
    //int doublein =10;
    int out = 2;
    int doublePipe = 13;
    int doubleAnd = 14;

    char* strin[2];
    char* strout[2];
    //char* strdoublein[4];
    char* strpiped[2];
    char* strDoublePipe[10];
    char* strDoubleAnd[10];

    piped = parsePipe(str, strpiped);
    in = parseRedirectIn(str, strin);
    out = parseRedirectOut(str, strout);
    doubleAnd = parseDoubleAnd(str, strDoubleAnd);
    doublePipe = parseDoublePipe(str, strDoublePipe);
    //doublein = parseDoubleRedirectIn(str, strdoublein);

    if (piped) {
        parseSpace(strpiped[0], parsed);
        parseSpace(strpiped[1], parsedpipe);
    }
    if (in) {
        parseSpace(strin[0], parsed);
        parseSpace(strin[1], strredin);
        //redirectionIn(strin[0],strin[1]);
        in = 3;
    }
    if (out) {
        parseSpace(strout[0], parsed);
        parseSpace(strout[1], strredout);
        out = 4;
    }
    //if (doublein) {
       // parseSpace(strdoublein[0], parsed);
       // parseSpace(strdoublein[1], doubleredin);

    if (doublePipe) {
        parseSpace(strDoublePipe[0], stringDoublePipe1);
        parseSpace(strDoublePipe[2], stringDoublePipe2);

        return 13;
    }
    if (doubleAnd) {
        parseSpace(strDoubleAnd[0], stringDoubleAnd1);
        parseSpace(strDoubleAnd[2], stringDoubleAnd2);

        return 14;
    }
    else {
        parseSpace(str, parsed);
    }

    if (ownCmdHandler(parsed))
        return 0;
    else
        return 1 + piped + in + out;
}

