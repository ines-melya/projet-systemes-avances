#ifndef PARSED_H_
#define PARSED_H_

int parseDoublePipe(char* str, char** strDoublePipe);
int parseDoubleAnd(char* str, char** strDoubleAnd);
int parseDoubleRedirectIn(char* str, char** strdoublein);
int parseRedirectIn(char* str, char** strredin);
int parseRedirectOut(char* str, char** strredout);
int parsePipe(char* str, char** strpiped);
void parseSpace(char* str, char** parsed);

#endif // PARSED_H_