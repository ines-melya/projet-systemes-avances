// C Program to design a shell in Linux
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include<fcntl.h>
#include<readline/readline.h>
#include<readline/history.h>
#include<errno.h>

#include "Utils.h"
#include "Parsed.h"
#include "Exec.h"
#include "Builtin.h"

#define MAXCOM 1000 // max number of letters to be supported
#define MAXLIST 100 // max number of commands to be supported

// Clearing the shell using escape sequences
#define clear() printf("\033[H\033[J")

//Variables globales
FILE *historyFile;
char path[] = "history.txt";
// Function to take input

/**
 * retrieve the entered command
 * 
 * @param str            Command string.
 * @return               true(1) if readline is ok or false(0) if readline failed
 */
int takeInput(char* str)
{
    char* buf;
    buf = readline("\nPrompt> ");
    if (strlen(buf) != 0) {
        add_history(buf);
        strcpy(str, buf);
        return 0;
    }
    else {
        return 1;
    }
}

/**
 * Greeting shell during startup
 * 
 */
void init_shell()
{
    clear();
    printf("\n\n\n\n******************"
        "************************");
    printf("\n\n\n\t**** SHELL In C****");
    printf("\n\n\t-Initialisation....-");
    printf("\n\n\n\n*******************"
        "***********************");
    char* username = getenv("USER");
    printf("\n\n\nWelcome: @%s", username);
    printf("\n");
    sleep(1);
    clear();

    //Ouverture 1er fichier
    historyFile = fopen(path, "w+");
 
    if(historyFile==NULL){
        printf("Erreur lors de l'ouverture d'un fichier");
        exit(1);
    }
}

/**
 * Adding the command to the history file
 * 
 * @param command        command to add on history
 */
void writeOnHistoryFile(char *command){
    if (historyFile == NULL)
      exit(1);
 
    for (int i = 0; command[i]!='\0'; i++){
        // Input string into the file
        // single character at a time
        fputc(command[i], historyFile);
    }
    //Retour à la ligne
    fputc(10, historyFile);
}

/**
 * Affiche l'historique.
 * 
 */
void printHistory(){
    int ch;

    fclose(historyFile);
    historyFile = fopen(path, "r");

    if(historyFile == NULL){
        printf("Erreur lors de l'ouverture d'un fichier");
        exit(1);
    }
 
    while((ch = fgetc(historyFile))!=EOF){
        printf("%c", ch);
    }

    printf("\n");

    fclose(historyFile);
    historyFile = fopen(path, "w+");
}

/**
 * Close and delete the history file
 * 
 */
void closeAndDeleteFile(){
    fclose(historyFile);
    if (remove(path) != 0){
        printf("Impossible de supprimer le fichier %s\n", path);
        perror("Erreur");    
    }
}

/**
 * Function to print Current Directory.
 * 
 */
void printDir()
{
    char cwd[1024];
    getcwd(cwd, sizeof(cwd));
    printf("\nDir: %s", cwd);
}

/**
 * Help command builtin
 * 
 */
void openHelp()
{
    puts("\n***this is the help panel***"
        "\n*****************************"
        "\n-Use the shell at your own risk..."
        "\nList of Commands supported:"
        "\n>simple and double redirections"
        "\n>Builtin : cd,echo,exit"
        "\n>Pipe and Double Pipe"
        "\n> general commands available in a shell"
        "\n>space handling");

    return;
}

/**
 * analyzes the type of execution required
 * 
 * @param str                   inital command
 * @param parsed                table with the different parts of the command after parsing command words
 * @param parsedpipe            table with the different parts of the command if it's a piped command
 * @param strredin              table with the different parts of the command if it's a redirect in command
 * @param strredout             table with the different parts of the command if it's a redirect out command
 * @param stringDoubleAnd1      table with the different parts of the first command if it's a double and command
 * @param stringDoubleAnd2      table with the different parts of the second command if it's a double and command
 * @param stringDoublePipe1     table with the different parts of the first commands if it's a double piped command
 * @param stringDoublePipe2     table with the different parts of the second commands if it's a double piped command
 * @return                      the number corresponding to the type of the command
 */