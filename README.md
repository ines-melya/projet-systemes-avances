# Projet systèmes avancés

Commande pour exécuter le shell: 

 gcc Utils.c Parsed.c Exec.c Builtin.c Boucle_Principale.c -lreadline


 Fonctionnalités : 

 Builtin (cd , exit, echo,help)
 General commands
 Redirections (double and simple) 
 Documentation
 Pipe Handling (pas plus que 2 pipes, comme spécifié dans le sujet)

